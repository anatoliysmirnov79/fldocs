/*
  Создать новый документ в проекте
  	input_params:
  		{
    		"user_id": int,
    		"user_lang_id": int,
    		"params": {
        		"project_id" int,
        		"parent_id" int,
        		"is_folder" int,
        		"name" string,
        		"type" int,
        		"body_attachment_id" int,
        		"tags" json,
        		"attachments" json
    		}
		}	
		
		* user_id 	   		 - ID пользователя(персоны), который запускает процедуру
		  user_lang_id 		 - ID языка пользователя(персоны), который запускает процедуру (null - берем его из базы)
		* params.project_id  - ID проекта, к которому относится документ
		  params.parent_id   - ID документа(фолдера), к которому относится создаваемый документ (null - корневой уровень)
		  params.is_folder   - 0/1 признак того, что запись является фолдером (по умолчанию - 1 это фолдер)
		* params.name 		 - наименование документа
		  params.type 		 - ID типа документа (справочник project_document_template_types)
		  params.body_attachment_id - ID аттачмента, хранящего тело документа
		  params.tags		 - JSON массив строк - поисковых тэгов данного документа
		  params.attachments - JSON массив ID-шников аттачей данного документа
		
		* - обязательное поле
	
	return:
		{
			"status": string,
			"message": string,
			"params": {
				"id": int
			}
		}
		
		status  	- результат работы процедуры ("ok"/"fail")
		message 	- сообщение об ошибке, если status = "fail"
		params.id 	- ID созданного документа
 
 	пример использования (создаем фолдер корневого уровня с именем myFolder2 в проекте с ID = 45):
 		call `create_project_document`('{"user_id": 1, "params": {"project_id": 45, "name": "myFolder2"}}');
 */
